Add to .htaccess:

  # Needed for SabreDAV
  RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization},L]
