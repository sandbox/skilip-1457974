<?php
// $Id$

/**
 * @file
 * Defines an 'Audio' Entity, used during the Wizzlern Entity training.
 */

/**
 * Extends Drupal's Default Entity Controller Class.
 */
class SabredavEntityController extends DrupalDefaultEntityController {

  /**
   * Saves or updates an entity.
   */
  public function save($entity, $entity_type) {
    if (empty($entity->{$this->idKey})) {
      // This is a new entity.
      drupal_write_record($this->entityInfo['base table'], $entity);
      field_attach_insert($entity_type, $entity);
      module_invoke_all('entity_insert', $entity_type, $entity);
    }
    else {
      // This is an existing entity.
      drupal_write_record($this->entityInfo['base table'], $entity, $this->idKey);
      field_attach_update($entity_type, $entity);
      module_invoke_all('entity_update', $entity_type, $entity);
    }
    return $entity;
  }

  /**
   * Deletes an entity.
   */
  public function delete($entity, $entity_type) {
    module_invoke_all('entity_delete', $entity_type, $entity);
    field_attach_delete($entity_type, $entity);
    db_delete($this->entityInfo['base table'])
      ->condition($this->idKey, $entity->{$this->idKey})
      ->execute();
  }
}
