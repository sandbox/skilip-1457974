<?php
// $Id$

/**
 * @file
 * Provide views data and handlers for audio_entity.module
 */

/**
 * Implements hook_views_data().
 */
function sabredav_views_data() {

  $data['sabredav_calendars'] = array();

  // Write the table definition for this data structure.
  $data['sabredav_calendars']['table'] = array(
    'group' => t('Calendar'),
    'base' =>  array(
      'field' => 'id',
      'title' => t('Calendars'),
      'help' => t('Calendars on your site.'),
    ),
  );
  $data['sabredav_calendars']['displayname'] = array(
    'title' => t('Display name'),
    'help' => t('The Display name of event.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['sabredav_calendars']['calendarcolor'] = array(
    'title' => t('Color'),
    'help' => t('The color of the calendar.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['sabredav_calendars']['uid'] = array(
    'title' => t('User'), 
    'help' => t('User uid.'),
    'relationship' => array(
      'title' => t('Calendar owner'),
      'help' => t('Relate the calendar to the user who created it. This relationship will create one record for each content item created by the user.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('Calendars'),
    ),
  );


  $data['sabredav_calendar_events'] = array();

  // Write the table definition for this data structure.
  $data['sabredav_calendar_events']['table'] = array(
    'group' => t('Event'), // This will be displayed in the Views UI.
    'base' =>  array(
      'field' => 'id', // The unique ID for our database structure.
      'title' => t('Events'),
      'help' => t('Events on your site.'),
    ),
  );

  $data['sabredav_calendar_events']['id'] = array(
    'title' => t('Event id'),
    'help' => t('The id of the event.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  // The event Title field.
  $data['sabredav_calendar_events']['calendarid'] = array(
    'title' => t('Calendar Id'),
    'help' => t('The id of the calendar to which the event belongs.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'title' => t('Calendar'),
      'help' => t('Relate event to calendar.'),
      'handler' => 'views_handler_relationship',
      'base' => 'sabredav_calendars',
      'base field' => 'id',
      'field' => 'calendarid',
      'label' => t('Calendar'),
    ),
  );
  $data['sabredav_calendar_events']['summary'] = array(
    'title' => t('Summary'),
    'help' => t('The summary of event.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  return $data;
}
