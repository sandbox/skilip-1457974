<?php
// $Id$

/**
 * @file
 *
 * @author skilip
 */

/**
 *
 */
function sabredav_user_calendar_overview() {
  // Build the sortable table header.
  $header = array(
    'displayname' => array('data' => t('Title'), 'field' => 'c.displayname'),
    'operations' => array('data' => t('Operations')),
  );
  $rows = array();

  $query = db_select('sabredav_calendars', 'c')
    ->extend('PagerDefault')
    ->extend('TableSort');

  $ids = $query
    ->fields('c', array('id'))
    ->limit(50)
    ->orderByHeader($header)
    ->execute()
    ->fetchCol();

  if (!empty($ids)) {
    foreach (sabredav_calendar_load_multiple($ids) as $sabredav_calendar) {

      $uri = entity_uri('sabredav_calendar', $sabredav_calendar);

      $rows[$sabredav_calendar->id]['displayname'] = array(
        'data' => array(
          '#markup' => $sabredav_calendar->displayname,
        ),
      );
      $rows[$sabredav_calendar->id]['operations'] = array(
        'data' => array(
          '#theme' => 'links',
          '#attributes' => array('class' => array('links', 'inline')),
          '#links' => array(
            'edit' => array(
              'title' => t('edit'),
              'href' => $uri['path'] . '/edit',
            ),
            'delete' => array(
              'title' => t('delete'),
              'href' => $uri['path'] . '/delete',
            ),
          ),
        ),
      );
    }
  }
  $build['audio_entities'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No calendars available yet.'),
  );
  $build['pager'] = array('#markup' => theme('pager'));
  return $build;
}

/**
 *
 */
function sabredav_calendar_form($form, &$form_state, $sabredav_calendar = NULL, $account = NULL) {
  // Create an empty entity if we're creating a new audio item instead of
  // editing an existing one.
  if (empty($sabredav_calendar)) {
    $sabredav_calendar = (object) array(
      'id' => NULL,
      'principaluri' => '',
      'uri' => '',
      'displayname' => '',
      'description' => '',
      'calendarcolor' => '',
      'components' => '',
    );
  }

  // Store the calendar and the user object.
  $form['#sabredav_calendar'] = $sabredav_calendar;
  $form['#account'] = $account;

  // This cannot be dynamic!
  $form['principaluri'] = array(
    '#type' => 'value',
    '#value' => 'principals/' . $account->name,
  );
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $sabredav_calendar->id,
  );

  $form['displayname'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $sabredav_calendar->displayname,
    '#required' => TRUE,
  );
  $form['uri'] = array(
    '#type' => 'machine_name',
    '#title' => t('URL path'),
    '#default_value' => $sabredav_calendar->uri,
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'sabredav_calendar_url_exists',
      'source' => array('displayname'),
      'label' => t('URL path'),
      'replace_pattern' => '[^a-z0-9-]+',
      'replace' => '-',
    ),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $sabredav_calendar->description,
  );
  $form['calendarcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Calendar color'),
    '#default_value' => str_replace('#', '', $sabredav_calendar->calendarcolor),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#required' => TRUE,
  );
  $form['enable_todos'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable \'to-do\' items'),
    '#default_value' => (bool) strstr($sabredav_calendar->components, 'VTODO'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  // Attach fields for this entity.
  field_attach_form('sabredav_calendar', $sabredav_calendar, $form, $form_state);

  return $form;
}

/**
 *
 */
function sabredav_calendar_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  // Get the original audio item.
  $sabredav_calendar = $form['#sabredav_calendar'];
  $account = $form['#account'];

  $timezone = array(
    'BEGIN:VCALENDAR',
    'VERSION:2.0',
    'PRODID:-//Apple Inc.//iCal 5.0.2//EN',
    'CALSCALE:GREGORIAN',
    'BEGIN:VTIMEZONE',
    'TZID:Europe/Amsterdam',
    'BEGIN:DAYLIGHT',
    'TZOFFSETFROM:+0100',
    'RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU',
    'DTSTART:19810329T020000',
    'TZNAME:CEST',
    'TZOFFSETTO:+0200',
    'END:DAYLIGHT',
    'BEGIN:STANDARD',
    'TZOFFSETFROM:+0200',
    'RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU',
    'DTSTART:19961027T030000',
    'TZNAME:CET',
    'TZOFFSETTO:+0100',
    'END:STANDARD',
    'END:VTIMEZONE',
    'END:VCALENDAR',
  );


  // Remove unneeded values.
  form_state_values_clean($form_state);
  // Copy submitted values to entity properties.
  entity_form_submit_build_entity('sabredav_calendar', $sabredav_calendar, $form, $form_state);

  // Merge new values into the original entity.
  $sabredav_calendar = (object) array_intersect_key((array) $sabredav_calendar, $values);

  $sabredav_calendar->calendarcolor  = '#' . $sabredav_calendar->calendarcolor;
  $sabredav_calendar->components = ($sabredav_calendar->enable_todos ? 'VEVENT,VTODO' : 'VEVENT');
  $sabredav_calendar->timezone = implode(' ', $timezone);


  // Save the entity.
  sabredav_calendar_save($sabredav_calendar);

  // Clear chaches
  cache_clear_all();

  drupal_set_message(t('The changes have been saved.'));
  $form_state['redirect'] = 'user/' . $account->uid . '/calendars';
}

/**
 *
 */
function sabredav_calendar_event_form($form, &$form_state, $sabredav_calendar_event = NULL, $account = NULL) {

  // Get the calendars for the current user.
  $sabredav_calendars = sabredav_calendar_load_by_uid(($account ? $account->uid : $GLOBALS['user']->uid));
  $sabredav_calendar_options = array();
  foreach ($sabredav_calendars as $sabredav_calendar) {
    $sabredav_calendar_options[$sabredav_calendar->id] = $sabredav_calendar->displayname;
  }

  // Create an empty entity if we're creating a new event instead of
  // editing an existing one.
  if (empty($sabredav_calendar_event)) {

    libraries_load('sabredav');

    $calendar_ids = array_keys($sabredav_calendar_options);
    $sabredav_calendar_event = (object) array(
      'id' => NULL,
      'uri' => strtoupper(Sabre_DAV_UUIDUtil::getUUID()) . '.ics',
      'summary' => '',
      'calendarid' => reset($calendar_ids),
      'calendardata' => '',
    );
  }

  // Store the event and the user object.
  $form['#sabredav_calendar_event'] = $sabredav_calendar_event;

  // This cannot be dynamic!
  $form['uri'] = array(
    '#type' => 'value',
    '#value' => $sabredav_calendar_event->uri,
  );
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $sabredav_calendar_event->id,
  );

  $form['summary'] = array(
    '#type' => 'textfield',
    '#title' => t('Summary'),
    '#default_value' => $sabredav_calendar_event->summary,
    '#required' => TRUE,
  );
  $form['calendarid'] = array(
    '#type' => 'select',
    '#title' => t('Calendar'),
    '#options' => $sabredav_calendar_options,
    '#default_value' => $sabredav_calendar_event->calendarid,
    '#access' => (count($sabredav_calendar_options) > 1),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  // Attach fields for this entity.
  field_attach_form('sabredav_calendar_event', $sabredav_calendar_event, $form, $form_state);

  return $form;
}

/**
 *
 */
function sabredav_calendar_event_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  // Get the original event.
  $sabredav_calendar_event = $form['#sabredav_calendar_event'];

  // Remove unneeded values.
  form_state_values_clean($form_state);
  // Copy submitted values to entity properties.
  entity_form_submit_build_entity('sabredav_calendar_event', $sabredav_calendar_event, $form, $form_state);

  // Merge new values into the original entity.
  $sabredav_calendar_event = (object) array_intersect_key((array) $sabredav_calendar_event, $values);

  $sabredav_calendar_event->lastmodified = REQUEST_TIME;

  // Create the caldav formatted calendar data.
  sabredav_calendar_event_populate_calendardata($sabredav_calendar_event);
  // Save the entity.
  sabredav_calendar_event_save($sabredav_calendar_event);

  // Clear chaches
  cache_clear_all();

  drupal_set_message(t('The changes have been saved.'));
}

/**
 * Menu callback -- ask for confirmation of audio item deletion.
 */
function sabredav_calendar_delete_confirm($form, &$form_state, $sabredav_calendar) {
  $form['id'] = array('#type' => 'value', '#value' => $sabredav_calendar->id);
  $uri = entity_uri('sabredav_calendar', $sabredav_calendar);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array(
      '%title' => $sabredav_calendar->displayname,
    )),
    substr($uri['path'], 0, strrpos($uri['path'], '/')),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute audio item deletion
 */
function sabredav_calendar_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $sabredav_calendar = sabredav_calendar_load($form_state['values']['id']);
    $uri = entity_uri('sabredav_calendar', $sabredav_calendar);
    sabredav_calendar_delete($sabredav_calendar);
    drupal_set_message(t('%title has been deleted.', array(
      '%title' => $sabredav_calendar->displayname,
    )));
  }
  $form_state['redirect'] = substr($uri['path'], 0, strrpos($uri['path'], '/'));
}

/**
 *
 */
function sabredav_calendar_settings_form($form, &$form_state) {
  return system_settings_form($form);
}

/**
 *
 */
function sabredav_calendar_event_settings_form($form, &$form_state) {
  return system_settings_form($form);
}

/**
 *
 */
function sabredav_addressbook_settings_form($form, &$form_state) {
  return system_settings_form($form);
}

/**
 *
 */
function sabredav_addressbook_contact_settings_form($form, &$form_state) {
  return system_settings_form($form);
}
