(function($) {
Drupal.fullcalendar.plugins.your_source_custom = {
  options: function (fullcalendar) {
    return {
			editable: true,
			selectable: true,
			select: function(start, end, allDay) {
				var title = prompt('Event Title:');
				if (title) {
					$('.fullcalendar', fullcalendar.$calendar).fullCalendar('renderEvent',
						{
							title: title,
							start: start,
							end: end,
							color: 'yellow',
							allDay: allDay
						},
						true // make the event "stick"
					);
				}
				$('.fullcalendar', fullcalendar.$calendar).fullCalendar('unselect');
			}
    };
  }
};

/**
 * Parse Drupal events from the DOM.
 */
Drupal.fullcalendar.fullcalendar.prototype.parseEvents = function (callback) {
  var events = [];
  var details = $('.fullcalendar-event-details', this.$calendar);
  for (var i = 0; i < details.length; i++) {
    var event = $(details[i]);
    events.push({
      field: event.attr('field'),
      index: event.attr('index'),
      eid: event.attr('eid'),
      entity_type: event.attr('entity_type'),
      title: event.attr('title'),
      start: event.attr('start'),
      end: event.attr('end'),
      url: event.attr('href'),
      color: event.attr('color'),
      allDay: (event.attr('allDay') === '1'),
      className: event.attr('cn'),
      editable: (event.attr('editable') === '1'),
      dom_id: this.dom_id
    });
  }
  console.log(events);
  callback(events);
};

}(jQuery));