<?php
// $Id$

/**
 * @file
 * SabreDAV integration
 *
 * @name SabreDAV
 * @author admin 
  */

/*
 * Implements hook_schema().
 */
function sabredav_schema() {
  $schema['sabredav_calendars'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'principaluri' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'displayname' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'uri' => array(
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'ctag' => array(
         'type' => 'int',
         'unsigned' => TRUE,
         'not null' => TRUE,
         'default' => 0,
      ),
      'description' => array(
        'type' => 'text',
        'not null' => TRUE,
      ),
      'uid' => array(
         'type' => 'int',
         'unsigned' => TRUE,
         'not null' => TRUE,
         'default' => 0,
      ),
      'calendarorder' => array(
         'type' => 'int',
         'unsigned' => TRUE,
         'not null' => TRUE,
         'default' => 0,
      ),
      'calendarcolor' => array(
        'type' => 'varchar',
        'length' => 10,
        'not null' => FALSE,
      ),
      'timezone' => array(
        'type' => 'text',
        'not null' => TRUE,
      ),
      'components' => array(
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array(
      'id',
    ),
  );
  $schema['sabredav_calendar_events'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'calendardata' => array(
        'type' => 'blob',
        'not null' => TRUE,
      ),
      'uri' => array(
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'summary' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'calendarid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'lastmodified' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array(
      'id',
    ),
  );
  $schema['sabredav_addressbooks'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'principaluri' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'displayname' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'uri' => array(
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'uid' => array(
         'type' => 'int',
         'unsigned' => TRUE,
         'not null' => TRUE,
         'default' => 0,
      ),
      'description' => array(
        'type' => 'text',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array(
      'id',
    ),
  );
  $schema['sabredav_addressbook_contacts'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'addressbookid' => array(
         'type' => 'int',
         'unsigned' => TRUE,
         'not null' => TRUE,
         'default' => 0,
      ),
      'carddata' => array(
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
      ),
      'uri' => array(
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'lastmodified' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array(
      'id',
    ),
  );
  $schema['sabredav_groupmembers'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'principal_id' => array(
         'type' => 'int',
         'unsigned' => TRUE,
         'not null' => TRUE,
         'default' => 0,
      ),
      'member_id' => array(
         'type' => 'int',
         'unsigned' => TRUE,
         'not null' => TRUE,
         'default' => 0,
      ),
    ),
    'primary key' => array(
      'id',
    ),
  );
  $schema['sabredav_principals'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uri' => array(
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'email' => array(
        'type' => 'varchar',
        'length' => 80,
        'not null' => FALSE,
      ),
      'displayname' => array(
        'type' => 'varchar',
        'length' => 80,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array(
      'id',
    ),
  );
  $schema['sabredav_user_digesta1'] = array(
    'fields' => array(
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'digesta1' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ),
    ),
  );
  return $schema;
}

/**
 * Implementation of hook_enable().
 */
function sabredav_enable() {
  // Check if our field is not already created.
  if (!field_info_field(SABREDAV_CALENDAR_EVENT_DATE_FIELD)) {

    $field = array(
      'field_name' => SABREDAV_CALENDAR_EVENT_DATE_FIELD, 
      'type' => 'datetime', 
      'settings' => array(
        'todate' => 'required',
        'repeat' => 1,
      ),
    );
    field_create_field($field);

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => SABREDAV_CALENDAR_EVENT_DATE_FIELD,
      'entity_type' => 'sabredav_calendar_event',
      'label' => 'Date',
      'bundle' => 'sabredav_calendar_event',
      'required' => TRUE,
      'locked' => TRUE,
      'widget' => array(
        'type' => 'date_select_repeat',
      ),
    );
    field_create_instance($instance);
  }
}

/**
 * Implementation of hook_enable().
 */
function sabredav_disable() {
  field_delete_field(SABREDAV_CALENDAR_EVENT_DATE_FIELD);
}
